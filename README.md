# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This solution designed to address three requirements:

- List vehicles with their status
- List vehicles for specific customer
- List vehicles based on their status

To address the mentioned requirements micro-services architecture. Each entity has its own service dedicated (Customer, Vehicle).
So that they can be developed individually. 

#### The structure of repo ####

- ApiGateway:
  - Ocelot used as a gateway
  - Aggregator which combines some of the services functionality to avoid increasing the communication
- Services: contains all services implementations
  - [Customer.API](#####Customer.API): implement the customer related functionalities like querying customers or creating them
  - [Car.API](#####Car.API): contains stores the cars along with their status
  - DummyService: which used as pinging services to send alive message to car
  - Identity.API: the centralized authentication service

##### Customer.API #####

This service uses MongoDB as database and multi layer architecture implemented in this service.

**_Remaining Work:_**

There are few unit test are implemented so far. Functional/Integration tests should be implemented.

Identity integration is unfinished.

---

##### Car.API #####

The implementation of this service is similar to [Customer.API](#####Customer.API).

**_Remaining Work:_**

There are only endpoint regarding the filtering the data. No implementation for the inserting or updating besides the status of the car.

No unit test implemented so far. Functional/Integration tests should be implemented.

Identity integration is left.

The plan for communication between customer and vehicle was using RabbitMQ for cases like deleting a customer.

---


### How do I get set up? ###

To run the project use the docker-compose up command.

Ports:

- Customer.API: 8000
- Car.API: 8001
- Gateway: 8010
- Aggregator: 8020

