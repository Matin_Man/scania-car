﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using DummySignal.Services.Interfaces;

namespace DummySignal.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly HttpClient _client;
        public VehicleService(HttpClient client
            )
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<bool> RenewStatus(string vehicleId)
        {
            var response = await _client.PatchAsync($"/api/v1/CustomerVehicle/{vehicleId}/alive", null);

            return response.StatusCode == System.Net.HttpStatusCode.OK;
        }
    }
}
