﻿using System;
using System.Threading.Tasks;

namespace DummySignal.Services.Interfaces
{
    public interface IVehicleService
    {
        Task<bool> RenewStatus(string vehicleId);
    }
}
