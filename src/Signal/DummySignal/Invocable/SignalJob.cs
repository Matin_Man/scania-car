﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Coravel.Invocable;
using DummySignal.Services.Interfaces;
using Microsoft.Extensions.Logging;

namespace DummySignal.Invocable
{
    public class SignalJob : IInvocable
    {
        private readonly ILogger<SignalJob> _logger;
        private readonly IVehicleService _vehicleService;
        private readonly Random _random = new Random();
        // should be retrieved from a data source
        List<string> carIds;
        public SignalJob(ILogger<SignalJob> logger, IVehicleService vehicleService)
        {
            _logger = logger;
            _vehicleService = vehicleService ?? throw new ArgumentNullException(nameof(vehicleService));

            carIds = new List<string>
            {
                "123456789098765432123101",
                "123456789098765432123102",
                "123456789098765432123103",
                "123456789098765432123104",
                "123456789098765432123105",
                "123456789098765432123106",
                "123456789098765432123107",
            };
        }

        public async Task Invoke()
        {
            await Task.Delay(1);
            foreach (var carId in carIds)
            {
                int num = _random.Next(10);
                _logger.LogInformation("Randome Number is {num}", num);
                //Console.WriteLine($"Randome Number is {num}");
                if (num > 4)
                {
                    var result = await _vehicleService.RenewStatus(carId);
                    _logger.LogInformation("fire {carId} event -> resut = {result}", carId, result);
                    //Console.WriteLine($"fire {carId} event -> resut = {result}");
                }
                else
                {
                    _logger.LogInformation("fire {carId} event FAILED!!!!!", carId);
                    //Console.WriteLine($"fire {carId} event FAILED!!!!!");
                }
            }
        }

    }
}
