﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Customer.API.Entities;
using Customer.API.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Customer.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    //[Authorize]
    public class CustomerController : Controller
    {
        private readonly ICustomerRepository _repository;


        public CustomerController(ICustomerRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<CustomerEntity>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<CustomerEntity>>> GetCustomers()
        {
            return Ok(await _repository.GetCustomers());
        }

        [HttpGet("{id:length(24)}", Name = "GetCustomer")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(CustomerEntity), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<CustomerEntity>> GetCustomerById(string id)
        {
            var customer = await _repository.GetCustomer(id);

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        [HttpPost]
        [ProducesResponseType(typeof(CustomerEntity), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<CustomerEntity>> CreateCustomer(CustomerEntity customer)
        {
            await _repository.CreateCustomer(customer);
            return CreatedAtRoute("GetCustomer", new { id = customer.Id }, customer);
        }

        [HttpPut]
        [ProducesResponseType(typeof(CustomerEntity), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> UpdateCustomer(CustomerEntity customer)
        {
            return Ok(await _repository.UpdateCustomer(customer));
        }

        [HttpDelete]
        [ProducesResponseType(typeof(CustomerEntity), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> DeleteCustomer(string id)
        {
            return Ok(await _repository.DeleteCustomer(id));
        }
    }
}
