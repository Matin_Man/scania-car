﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Customer.API.Entities;

namespace Customer.API.Repositories.Interfaces
{
    public interface ICustomerRepository
    {
        Task<IEnumerable<CustomerEntity>> GetCustomers();
        Task<CustomerEntity> GetCustomer(string id);
        Task CreateCustomer(CustomerEntity customer);
        Task<bool> UpdateCustomer(CustomerEntity customer);
        Task<bool> DeleteCustomer(string id);
    }
}
