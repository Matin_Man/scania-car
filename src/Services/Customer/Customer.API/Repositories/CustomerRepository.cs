﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Customer.API.Data.Interfaces;
using Customer.API.Entities;
using Customer.API.Repositories.Interfaces;
using MongoDB.Driver;

namespace Customer.API.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly ICustomerContext _context;

        public CustomerRepository(ICustomerContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task CreateCustomer(CustomerEntity customer)
        {
            await _context.Customers.InsertOneAsync(customer);
        }

        public async Task<bool> DeleteCustomer(string id)
        {
            var result = await _context.Customers.DeleteOneAsync(filter: c => c.Id == id);

            return result.IsAcknowledged && result.DeletedCount > 0;
        }

        public async Task<CustomerEntity> GetCustomer(string id)
        {
            return await _context.Customers.Find(c => c.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<CustomerEntity>> GetCustomers()
        {
            return await _context.Customers.Find(c => true).ToListAsync();
        }

        public async Task<bool> UpdateCustomer(CustomerEntity customer)
        {
            var result = await _context.Customers.ReplaceOneAsync(filter: c => c.Id == customer.Id, replacement: customer);

            return result.IsAcknowledged && result.ModifiedCount > 0;
        }
    }
}
