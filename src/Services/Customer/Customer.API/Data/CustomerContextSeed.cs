﻿using System;
using System.Collections.Generic;
using Customer.API.Entities;
using MongoDB.Driver;

namespace Customer.API.Data
{
    internal class CustomerContextSeed
    {
        internal static void SeedData(IMongoCollection<CustomerEntity> customers)
        {
            bool existCustomer = customers.Find(c => true).Any();

            if (!existCustomer)
            {
                var result = customers.InsertManyAsync(GetCustomers());
            }
        }

        private static IEnumerable<CustomerEntity> GetCustomers()
        {
            return new List<CustomerEntity>
            {
                new CustomerEntity
                {
                    Id = "123456789098765432123451",
                    Name = "Kallas AB",
                    City = "Sodertalje",
                    PostalCode = 11111,
                    Street = "Cementvagen 8"
                },
                new CustomerEntity
                {
                    Id = "123456789098765432123452",
                    Name = "Johans AB",
                    City = "Stockholm",
                    PostalCode = 22222,
                    Street = "Balkvagen 12"
                },
                new CustomerEntity
                {
                    Id = "123456789098765432123453",
                    Name = "Haralds AB",
                    City = "Uppsala",
                    PostalCode = 33333,
                    Street = "Budgetvagen 1"
                }
            };
        }
    }
}
