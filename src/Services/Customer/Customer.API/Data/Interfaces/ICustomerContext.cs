﻿using System;
using Customer.API.Entities;
using MongoDB.Driver;

namespace Customer.API.Data.Interfaces
{
    public interface ICustomerContext
    {
        IMongoCollection<CustomerEntity> Customers { get; }
    }
}
