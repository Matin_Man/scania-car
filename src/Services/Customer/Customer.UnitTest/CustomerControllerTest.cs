﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Customer.API.Controllers;
using Customer.API.Entities;
using Customer.API.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Customer.UnitTest
{
    public class CustomerControllerTest
    {
        private readonly Mock<ICustomerRepository> _customerRepositoryMock;

        public CustomerControllerTest()
        {
            _customerRepositoryMock = new Mock<ICustomerRepository>();
        }


        [Fact]
        public async Task GetCustomerTestAsyncSuccessTest()
        {
            _customerRepositoryMock.Setup(c => c.GetCustomers()).Returns(Task.FromResult(GetFakeCustomers()));

            var customerController = new CustomerController(_customerRepositoryMock.Object);

            var actionResult = await customerController.GetCustomers();

            Assert.Equal((actionResult.Result as OkObjectResult).StatusCode, (int)System.Net.HttpStatusCode.OK);
        }

        [Fact]       
        public async Task GetCustomerByIdNotFoundTest()
        {
            _customerRepositoryMock.Setup(c => c.GetCustomer(It.IsAny<string>())).Returns(
                Task.FromResult<CustomerEntity>(GetFakeCustomers().FirstOrDefault(c=>c.Id=="None"))); ;

            var customerController = new CustomerController(_customerRepositoryMock.Object);

            var actionResult = await customerController.GetCustomerById("fakeId");
            Assert.Equal((actionResult.Result as StatusCodeResult).StatusCode, (int)System.Net.HttpStatusCode.NotFound);
        }

        // refactor duplication or use theory
        [Fact]
        public async Task GetCustomerByIdFoundTest()
        {
            _customerRepositoryMock.Setup(c => c.GetCustomer(It.IsAny<string>())).Returns(
                Task.FromResult<CustomerEntity>(GetFakeCustomers().FirstOrDefault(c => c.Id == "id"))); ;

            var customerController = new CustomerController(_customerRepositoryMock.Object);

            var actionResult = await customerController.GetCustomerById("fakeId");
            Assert.Equal((actionResult.Result as OkObjectResult).StatusCode, (int)System.Net.HttpStatusCode.OK);
        }

        private IEnumerable<CustomerEntity> GetFakeCustomers()
        {
            return new List<CustomerEntity>
            {
                new CustomerEntity
                {
                     Name="name",
                     City="city",
                     PostalCode=11111,
                     Id="id"
                }
            };
        }
    }
}
