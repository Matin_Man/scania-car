﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Car.API.Model;

namespace Car.API.Repository.Interface
{
    public interface IVehicleRepository
    {
        Task<IEnumerable<Vehicle>> GetVehicles(string ownerId);

        Task<IEnumerable<Vehicle>> GetVehicles(bool status);

        Task<bool> RenewStatus(string vehicleId, bool status);
    }
}
