﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Car.API.Data;
using Car.API.Model;
using Car.API.Repository.Interface;
using MongoDB.Driver;

namespace Car.API.Repository
{
    public class VehicleRepository : IVehicleRepository
    {
        private readonly IVehicleContext _vehicleContext;

        public VehicleRepository(IVehicleContext vehicleContext)
        {
            _vehicleContext = vehicleContext?? throw new ArgumentNullException(nameof(vehicleContext));
        }

        public async Task<IEnumerable<Vehicle>> GetVehicles(string ownerId)
        {
            var now = DateTime.UtcNow.AddMinutes(-1);
            var vehicles = await _vehicleContext.Vehicles.Find(v => v.OwnerId == ownerId).ToListAsync();

            SetStatus(vehicles, now);
            return vehicles;
        }

        public async Task<IEnumerable<Vehicle>> GetVehicles(bool status)
        {
            var now = DateTime.UtcNow.AddMinutes(-1);

            IEnumerable<Vehicle> vehicles = null;

            if (status)
            {
                vehicles = await _vehicleContext.Vehicles.Find<Vehicle>(filter: v => now <= v.StatusReceivedTime).ToListAsync();
            }
            else
            {
                vehicles = await _vehicleContext.Vehicles.Find<Vehicle>(filter: v => now > v.StatusReceivedTime || v.StatusReceivedTime == null).ToListAsync();
            }
            SetStatus(vehicles, now);

            return vehicles;
        }

        public async Task<bool> RenewStatus(string vehicleId, bool status)
        {
            var update = Builders<Vehicle>.Update.Set(v => v.Status, status)
                                                 .Set(v => v.StatusReceivedTime, DateTime.UtcNow);
            var options = new FindOneAndUpdateOptions<Vehicle>{ ReturnDocument = ReturnDocument.Before, IsUpsert=false };

            var result = await _vehicleContext.Vehicles.FindOneAndUpdateAsync(filter: v => v.Id == vehicleId, update);

            return result != null;
            
        }

        private void SetStatus(IEnumerable<Vehicle> vehicles, DateTime now)
        {
            foreach (var vehicle in vehicles)
            {
                SetStatus(vehicle, now);
            }
        }

        private void SetStatus(Vehicle vehicle, DateTime now)
        {
            vehicle.Status = true;
            if (!vehicle.StatusReceivedTime.HasValue || now.Subtract(vehicle.StatusReceivedTime.Value).Seconds > 1)
            {
                vehicle.Status = false;
            }
        }

    }

}