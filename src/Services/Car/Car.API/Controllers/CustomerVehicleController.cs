﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Car.API.Model;
using Car.API.Repository.Interface;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Car.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomerVehicleController : Controller
    {
        private readonly IVehicleRepository _repository;

        public CustomerVehicleController(IVehicleRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }


        [HttpGet("{ownerId}", Name = "GetVicleByStatus")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(CustomerVehicle), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<CustomerVehicle>> GetVehiclesById(string ownerId) 
        {
            var customerVehicle = await _repository.GetVehicles(ownerId);

            if(customerVehicle == null)
            {
                return NotFound();
            }

            return Ok(customerVehicle);
        }


        [HttpGet("status/{status}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(CustomerVehicle), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<CustomerVehicle>> GetCustomerVehicle(bool status)
        {
            var customerVehicle = await _repository.GetVehicles(status);

            if (customerVehicle == null)
            {
                return NotFound();
            }

            return Ok(customerVehicle);
        }

        [HttpPatch]
        [Route("{id}/alive")]
        [ProducesResponseType(typeof(Vehicle), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> UpdateVehicle(string id)
        {
            var result = await _repository.RenewStatus(id, true);

            return Ok(result);
        }
    }
}
