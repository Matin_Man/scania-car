﻿using System;
using Car.API.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Car.API.EntityConfiguration
{
    public class CustomerVehicleTypeConfiguration: IEntityTypeConfiguration<CustomerVehicle>
    { 
        public void Configure(EntityTypeBuilder<CustomerVehicle> builder)
        {
            builder.HasKey(b => b.Id);
            builder.Property(b => b.Id).ValueGeneratedOnAdd();
        }
    }
}
