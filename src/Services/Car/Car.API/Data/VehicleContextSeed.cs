﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Car.API.Model;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;

namespace Car.API.Data
{
    public static class VehicleContextSeed
    {

        public static void SeedData(IMongoCollection<Vehicle> vehicles)
        {
            bool existVehicles = vehicles.Find(v => true).Any();

            if (!existVehicles)
            {
                vehicles.InsertManyAsync(GetDummyCustomerVehicles());
            }
        }

        public static List<Vehicle> GetDummyCustomerVehicles()
        {

            return new List<Vehicle>
                {

                        new Vehicle
                        {
                             OwnerId = "123456789098765432123451",
                             Id="123456789098765432123101",
                             RegNumber = "reg 1",
                             VIN = "vin1"
                        },
                        new Vehicle
                        {
                            OwnerId = "123456789098765432123451",
                             Id="123456789098765432123102",
                             RegNumber = "reg 2",
                             VIN = "vin2"
                        },
                        new Vehicle
                        {
                            OwnerId = "123456789098765432123451",
                             Id="123456789098765432123103",
                             RegNumber = "reg 3",
                             VIN = "vin3"
                        }                ,

                        new Vehicle
                        {
                            OwnerId = "123456789098765432123452",
                            Id ="123456789098765432123104",
                             RegNumber = "reg 4",
                             VIN = "vin4"
                        },
                        new Vehicle
                        {
                            OwnerId = "123456789098765432123452",
                            Id ="123456789098765432123105",
                             RegNumber = "reg 5",
                             VIN = "vin5"
                        },
                        new Vehicle
                        {
                            OwnerId = "123456789098765432123452",
                             Id="123456789098765432123106",
                             RegNumber = "reg 6",
                             VIN = "vin6"
                        },
                        new Vehicle
                        {
                            OwnerId = "123456789098765432123452",
                             Id="123456789098765432123107",
                             RegNumber = "reg 7",
                             VIN = "vin7"
                        }

            };
        }

        //private static List<CustomerVehicle> GetCustomerVehiclesDummyData()
        //{
        //    return new List<CustomerVehicle>
        //    {
        //        new CustomerVehicle
        //        {
        //             Id=1,
        //             OwnerId = "123456789098765432123451"
        //        },
        //        new CustomerVehicle
        //        {
        //             Id=2,
        //             OwnerId = "123456789098765432123452"
        //        },
        //        new CustomerVehicle
        //        {
        //             Id=0,
        //             OwnerId = "123456789098765432123452"
        //        }
        //    };

        //}

        //public static async Task SeedAsync(VehicleContext context)
        //{
        //    if (!context.CustomerVehicles.Any())
        //    {
        //        context.CustomerVehicles.AddRange(GetDummyCustomerVehicles());

        //        await context.SaveChangesAsync();
        //    }
        //}

        //public static List<CustomerVehicle> GetDummyCustomerVehicles()
        //{
        //    return new List<CustomerVehicle>
        //    {
        //        new CustomerVehicle
        //        {
        //             OwnerId = "123456789098765432123451",
        //             Vehicles = new List<Vehicle>
        //             {
        //                new Vehicle
        //                {
        //                     CustomerVehicleId=1,
        //                     Id=1,
        //                     RegNumber = "reg 1",
        //                     VIN = "vin1"
        //                },
        //                new Vehicle
        //                {
        //                     CustomerVehicleId=1,
        //                     Id=2,
        //                     RegNumber = "reg 2",
        //                     VIN = "vin2"
        //                },
        //                new Vehicle
        //                {
        //                     CustomerVehicleId=1,
        //                     Id=3,
        //                     RegNumber = "reg 3",
        //                     VIN = "vin3"
        //                }
        //             }
        //        }
        //        ,
        //        new CustomerVehicle
        //        {
        //             Id=0,
        //             OwnerId = "123456789098765432123452",
        //             Vehicles = new List<Vehicle>
        //             {
        //                new Vehicle
        //                {
        //                     Id=0,
        //                     RegNumber = "reg 4",
        //                     VIN = "vin4"
        //                },
        //                new Vehicle
        //                {
        //                     Id=0,
        //                     RegNumber = "reg 5",
        //                     VIN = "vin5"
        //                }
        //             }
        //        },
        //        new CustomerVehicle
        //        {
        //             Id=0,
        //             OwnerId = "123456789098765432123452",
        //             Vehicles = new List<Vehicle>
        //             {
        //                new Vehicle
        //                {
        //                     Id=0,
        //                     RegNumber = "reg 6",
        //                     VIN = "vin6"
        //                },
        //                new Vehicle
        //                {
        //                     Id=0,
        //                     RegNumber = "reg 7",
        //                     VIN = "vin7"
        //                }
        //             }
        //        }

        //    };
        //}
    }
}
