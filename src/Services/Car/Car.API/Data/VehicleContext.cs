﻿using System;
using Car.API.Model;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace Car.API.Data
{
    public class VehicleContext : IVehicleContext
    {
        public VehicleContext(IConfiguration configuration)  
        {
            var client = new MongoClient(configuration.GetValue<string>("DatabaseSettings:ConnectionString"));
            var database = client.GetDatabase(configuration.GetValue<string>("DatabaseSettings:DatabaseName"));

            Vehicles = database.GetCollection<Vehicle>(configuration.GetValue<string>("DatabaseSettings:CollectionName"));

            VehicleContextSeed.SeedData(Vehicles);
        }


        public IMongoCollection<Vehicle> Vehicles { get; }


    }

}
