﻿using System;
using Car.API.Model;
using MongoDB.Driver;

namespace Car.API.Data
{
    public interface IVehicleContext
    {
        IMongoCollection<Vehicle> Vehicles { get; }
    }
}
