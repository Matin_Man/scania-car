﻿using System;
using System.Collections.Generic;

namespace Car.API.Model
{
    public class CustomerVehicle
    {
        public int? Id { get; set; }

        public string OwnerId { get; set; }

        public List<Vehicle> Vehicles { get; set; }
    }
}
