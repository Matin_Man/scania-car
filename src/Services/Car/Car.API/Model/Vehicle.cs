﻿using System;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Car.API.Model
{
    public class Vehicle
    {
        // TODO: Remove the status and find an alternative for ignore...
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string OwnerId { get; set; }

        public string VIN { get; set; }

        public string RegNumber { get; set; }

        public bool? Status { get; set; }

        [JsonIgnore]
        public DateTime? StatusReceivedTime { get; set; }
    }
}
