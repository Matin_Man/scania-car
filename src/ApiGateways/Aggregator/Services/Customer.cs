﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Aggregator.Helper;
using Aggregator.Models;
using Aggregator.Services.Interfaces;

namespace Aggregator.Services
{
    public class Customer : ICustomer
    {
        private readonly HttpClient _client;
        public Customer(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<CustomerResponseModel> GetCustomer(string id)
        {
            var response = await _client.GetAsync($"/api/v1/Customer/{id}");
            return await response.ReadContentAs<CustomerResponseModel>();
        }
    }
}
