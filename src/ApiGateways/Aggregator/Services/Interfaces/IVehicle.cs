﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aggregator.Models;

namespace Aggregator.Services.Interfaces
{
    public interface IVehicle
    {
        Task<IEnumerable<VehicleResponseModel>> GetVehicles(string ownerId);

        Task<IEnumerable<VehicleResponseModel>> GetVehicles(bool status);
    }
}
