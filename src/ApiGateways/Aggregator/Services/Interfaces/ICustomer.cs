﻿using System;
using System.Threading.Tasks;
using Aggregator.Models;

namespace Aggregator.Services.Interfaces
{
    public interface ICustomer
    {
        Task<CustomerResponseModel> GetCustomer(string id);
    }
}
