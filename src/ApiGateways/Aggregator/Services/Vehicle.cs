﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Aggregator.Models;
using Aggregator.Services.Interfaces;
using Aggregator.Helper;

namespace Aggregator.Services
{
    public class Vehicle : IVehicle
    {
        private readonly HttpClient _client;
        public Vehicle(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<IEnumerable<VehicleResponseModel>> GetVehicles(string ownerId)
        {
            var response = await _client.GetAsync($"/api/v1/CustomerVehicle/{ownerId}");
            return await response.ReadContentAs<List<VehicleResponseModel>>();
        }

        public async Task<IEnumerable<VehicleResponseModel>> GetVehicles(bool status)
        {
            var response = await _client.GetAsync($"/api/v1/CustomerVehicle/status/{status}");
            return await response.ReadContentAs<List<VehicleResponseModel>>();
        }
    }
}
