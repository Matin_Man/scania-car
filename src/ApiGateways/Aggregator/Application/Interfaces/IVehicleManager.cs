﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aggregator.Models;

namespace Aggregator.Application
{
    public interface IVehicleManager
    {
        Task<CustomerVehicles> GetCustomerVehicles(string customerId);

        Task<IEnumerable<VehicleResponseModel>> GetVehiclesById(string vehicleId);

        Task<IEnumerable<VehicleResponseModel>> GetVehicleByState(bool state);
    }
}
