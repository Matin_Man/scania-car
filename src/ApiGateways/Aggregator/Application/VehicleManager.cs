﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aggregator.Models;
using Aggregator.Services.Interfaces;

namespace Aggregator.Application
{
    public class VehicleManager : IVehicleManager
    {
        private readonly ICustomer _customerService;
        private readonly IVehicle _vehicleService;

        public VehicleManager(ICustomer customerService, IVehicle vehicleService)
        {
            _customerService = customerService ?? throw new ArgumentNullException(nameof(customerService));
            _vehicleService = vehicleService ?? throw new ArgumentNullException(nameof(vehicleService));
        }

        public async Task<CustomerVehicles> GetCustomerVehicles(string customerId)
        {
            var customer = await _customerService.GetCustomer(customerId);

            var vehicles = await _vehicleService.GetVehicles(customerId);

            var customerVehicles = new CustomerVehicles
            {
                Name = customer.Name,
                City = customer.City,
                Id = customer.Id,
                PostalCode = customer.PostalCode,
                Street = customer.Street,
                Vehicles = vehicles.ToList()
            };

            return customerVehicles;
        }

        public async Task<IEnumerable<VehicleResponseModel>> GetVehicleByState(bool state)
        {
           var vehicles = await _vehicleService.GetVehicles(state);

            return vehicles;
        }

        public async Task<IEnumerable<VehicleResponseModel>> GetVehiclesById(string vehicleId)
        {
            return await _vehicleService.GetVehicles(vehicleId);
        }
    }
}
