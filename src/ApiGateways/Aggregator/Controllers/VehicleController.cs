﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Aggregator.Application;
using Aggregator.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Aggregator.Controllers
{
   

    [ApiController]
    [Route("api/v1/[controller]")]
    public class VehicleController : Controller
    {
        private readonly IVehicleManager _vehicleManager;

        public VehicleController(IVehicleManager vehicleManager)
        {
            _vehicleManager = vehicleManager ?? throw new ArgumentNullException(nameof(vehicleManager));
        }

        [HttpGet("customer/{ownerId}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(CustomerVehicles), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<CustomerVehicles>> GetCustomerVehicles(string ownerId)
        {
            var customerVehicle = await _vehicleManager.GetCustomerVehicles(ownerId);

            if (customerVehicle == null)
            {
                return NotFound();
            }

            return Ok(customerVehicle);
        }

        [HttpGet("vehicle/{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(CustomerVehicles), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<VehicleResponseModel>> GetVehiclesById(string id)
        {
            var vehicles = await _vehicleManager.GetVehiclesById(id);

            if (vehicles == null)
            {
                return NotFound();
            }

            return Ok(vehicles);
        }

        [HttpGet("vehicle/state/{state}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(CustomerVehicles), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<VehicleResponseModel>> GetVehiclesById(bool state)
        {
            var vehicles = await _vehicleManager.GetVehicleByState(state);

            if (vehicles == null)
            {
                return NotFound();
            }

            return Ok(vehicles);
        }

    }

    
}
