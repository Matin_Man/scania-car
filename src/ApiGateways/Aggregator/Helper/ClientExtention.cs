﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Aggregator.Helper
{
    public static class ClientExtention
    { 
            public static async Task<T> ReadContentAs<T>(this HttpResponseMessage response)
            {
                if (!response.IsSuccessStatusCode)
                    throw new ApplicationException($"Service call failed: {response.ReasonPhrase}");

                var dataAsString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                return JsonSerializer.Deserialize<T>(dataAsString, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }
         
    }
}
