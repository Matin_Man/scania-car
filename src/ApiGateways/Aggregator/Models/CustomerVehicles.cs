﻿using System;
using System.Collections.Generic;

namespace Aggregator.Models
{
    public class CustomerVehicles
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public int PostalCode { get; set; }
        public string City { get; set; }

        public IEnumerable<VehicleResponseModel> Vehicles { get; set; }
    }
}
