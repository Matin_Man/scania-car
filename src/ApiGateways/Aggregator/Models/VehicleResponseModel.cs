﻿using System;
namespace Aggregator.Models
{
    public class VehicleResponseModel
    {
        public string Id { get; set; }
        public string OwnerId { get; set; }
        public string VIN { get; set; }
        public string RegNumber { get; set; }
        public bool Status { get; set; }
    }
}
